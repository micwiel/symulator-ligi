#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from random import randint
import cechy


class Osoba:
    """ Klasa podstawowa wszystkich wymodelowanych ludzi. """
    def __init__(self):
        self.imię = 'Adam'
        self.nazwisko = 'Nowak'
        self.wzrost = randint(150, 200)

    def __repr__(self):
        return "Osoba " + str([self.imię, self.nazwisko, self.wzrost])


class Piłkarz(Osoba):
    """ Klasa określająca piłkarza. """
    def __init__(self):
        super().__init__()
        self.cechy_fizyczne = cechy.fizyczne()
        self.cechy_psychiczne = cechy.psychiczne()
        self.cechy_techniczne = cechy.techniczne()
        self.bramka, self.obrona, self.pomoc, self.atak = \
            self.oblicz_współczynniki()  # metoda abstrakcyjna

    def __repr__(self):
        return "Piłkarz " + str([self.imię, self.nazwisko, self.wzrost])


class Bramkarz(Piłkarz):
    """ Do piłkarza dorzucamy cechy szczególne dla bramkarzy. """
    def __init__(self):
        self.cechy_bramkarskie = cechy.bramkarskie()
        super().__init__()

    def oblicz_współczynniki(self):
        """ Funkcja do wyliczania poszczególnych współczynników piłkarza
        (by uprościć algorytmy wyznaczające wyniki meczów). """
        # wszystkie cechy przemnożymy przez jakiś współczynnik w zależności od
        # pozycji piłkarza (niektóre cechy bardziej przydatne na danej pozycji)

        punkty = 0
        pośrednie = 0
        pośrednie += self.cechy_fizyczne['balans']
        pośrednie += self.cechy_fizyczne['siła']
        pośrednie += self.cechy_psychiczne['przewidywanie']
        pośrednie += self.cechy_psychiczne['opanowanie']
        pośrednie += self.cechy_psychiczne['koncentracja']
        pośrednie += self.cechy_psychiczne['stanowczość']
        pośrednie += self.cechy_techniczne['długie_podania']
        pośrednie += self.cechy_techniczne['przeciwdziałanie']
        pośrednie += self.cechy_psychiczne['odwaga']
        pośrednie += self.cechy_fizyczne['skoczność']
        pośrednie += self.cechy_psychiczne['autorytet']
        pośrednie += self.cechy_bramkarskie['komunikacja']
        pośrednie += self.cechy_bramkarskie['łapanie']
        punkty += pośrednie * 2.5  # wagi najwyższe
        pośrednie = 0
        pośrednie += self.cechy_fizyczne['przyśpieszenie']
        pośrednie += self.cechy_psychiczne['ustawianie']
        pośrednie += self.cechy_techniczne['podawanie']
        pośrednie += self.cechy_psychiczne['agresja']
        pośrednie += self.cechy_psychiczne['spryt']
        pośrednie += self.cechy_techniczne['rzuty_karne']
        pośrednie += self.cechy_bramkarskie['zachowanie_w_powietrzu']
        pośrednie += self.cechy_bramkarskie['dominacja']
        pośrednie += self.cechy_bramkarskie['sam_na_sam']
        pośrednie += self.cechy_bramkarskie['refleks']
        punkty += pośrednie * 2  # wagi wysokie
        pośrednie = 0
        pośrednie += self.cechy_techniczne['przyjęcie']
        pośrednie += self.cechy_fizyczne['kondycja']
        pośrednie += self.cechy_psychiczne['determinacja']
        pośrednie += self.cechy_techniczne['długie_strzały']
        pośrednie += self.cechy_fizyczne['szybkość']
        pośrednie += self.cechy_fizyczne['wytrzymałość']
        pośrednie += self.cechy_psychiczne['improwizowanie']
        pośrednie += self.cechy_bramkarskie['kopanie']
        pośrednie += self.cechy_bramkarskie['gwałtowność']
        pośrednie += self.cechy_bramkarskie['wyrzut']
        punkty += pośrednie * 1.5  # wagi średnie
        punkty += self.cechy_psychiczne['współdziałanie']
        punkty += self.cechy_techniczne['rzuty_wolne']
        punkty += self.cechy_techniczne['przechodzenie']
        punkty += self.cechy_psychiczne['pracowitość']
        punkty += self.cechy_techniczne['technika']
        punkty += self.cechy_techniczne['główkowanie']
        punkty += self.cechy_fizyczne['zwinność']
        punkty += self.cechy_psychiczne['kreatywność']
        punkty += self.cechy_techniczne['rzuty_rożne']
        punkty += self.cechy_techniczne['dryblowanie']
        punkty += self.cechy_techniczne['wykańczanie']
        punkty += self.cechy_bramkarskie['ekscentryczność']
        punkty += self.cechy_bramkarskie['skłonność_puszczania_piłki']
        return punkty * 0.75, punkty * 0.5 * 0.25, punkty * 0.37 * 0.25, \
            punkty * 0.13 * 0.25

    def __repr__(self):
        wynik = super().__repr__() + " Bramkarz"
        return wynik


class Obrońca(Piłkarz):
    def __init__(self):
        super().__init__()

    def oblicz_współczynniki(self):
        """ Funkcja do wyliczania poszczególnych współczynników piłkarza
        (by uprościć algorytmy wyznaczające wyniki meczów). """
        # wszystkie cechy przemnożymy przez jakiś współczynnik w zależności od
        # pozycji piłkarza (niektóre cechy bardziej przydatne na danej pozycji)

        punkty = 0
        pośrednie = 0
        pośrednie += self.cechy_fizyczne['przyśpieszenie']
        pośrednie += self.cechy_fizyczne['balans']
        pośrednie += self.cechy_fizyczne['siła']
        pośrednie += self.cechy_psychiczne['przewidywanie']
        pośrednie += self.cechy_psychiczne['opanowanie']
        pośrednie += self.cechy_psychiczne['koncentracja']
        pośrednie += self.cechy_psychiczne['stanowczość']
        pośrednie += self.cechy_psychiczne['ustawianie']
        pośrednie += self.cechy_psychiczne['współdziałanie']
        pośrednie += self.cechy_techniczne['przyjęcie']
        pośrednie += self.cechy_techniczne['rzuty_wolne']
        pośrednie += self.cechy_techniczne['długie_podania']
        pośrednie += self.cechy_techniczne['przeciwdziałanie']
        punkty += pośrednie * 2.5  # wagi najwyższe
        pośrednie = 0
        pośrednie += self.cechy_fizyczne['kondycja']
        pośrednie += self.cechy_psychiczne['determinacja']
        pośrednie += self.cechy_psychiczne['odwaga']
        pośrednie += self.cechy_techniczne['przechodzenie']
        pośrednie += self.cechy_techniczne['długie_strzały']
        pośrednie += self.cechy_techniczne['podawanie']
        punkty += pośrednie * 2  # wagi wysokie
        pośrednie = 0
        pośrednie += self.cechy_fizyczne['skoczność']
        pośrednie += self.cechy_fizyczne['szybkość']
        pośrednie += self.cechy_psychiczne['agresja']
        pośrednie += self.cechy_psychiczne['pracowitość']
        pośrednie += self.cechy_psychiczne['autorytet']
        pośrednie += self.cechy_techniczne['technika']
        pośrednie += self.cechy_techniczne['główkowanie']
        punkty += pośrednie * 1.5  # wagi średnie
        punkty += self.cechy_fizyczne['zwinność']
        punkty += self.cechy_fizyczne['wytrzymałość']
        punkty += self.cechy_psychiczne['kreatywność']
        punkty += self.cechy_psychiczne['spryt']
        punkty += self.cechy_psychiczne['improwizowanie']
        punkty += self.cechy_techniczne['rzuty_rożne']
        punkty += self.cechy_techniczne['dryblowanie']
        punkty += self.cechy_techniczne['wykańczanie']
        punkty += self.cechy_techniczne['rzuty_karne']
        return 0, punkty * 0.5, punkty * 0.37, punkty * 0.13

    def __repr__(self):
        wynik = super().__repr__() + " Obrońca"
        return wynik


class Pomocnik(Piłkarz):
    def __init__(self):
        super().__init__()

    def oblicz_współczynniki(self):
        """ Funkcja do wyliczania poszczególnych współczynników piłkarza
        (by uprościć algorytmy wyznaczające wyniki meczów). """
        # wszystkie cechy przemnożymy przez jakiś współczynnik w zależności od
        # pozycji piłkarza (niektóre cechy bardziej przydatne na danej pozycji)

        punkty = 0
        pośrednie = 0
        pośrednie += self.cechy_psychiczne['opanowanie']
        pośrednie += self.cechy_psychiczne['koncentracja']
        pośrednie += self.cechy_psychiczne['współdziałanie']
        pośrednie += self.cechy_techniczne['przyjęcie']
        pośrednie += self.cechy_fizyczne['kondycja']
        pośrednie += self.cechy_techniczne['przechodzenie']
        pośrednie += self.cechy_techniczne['długie_strzały']
        pośrednie += self.cechy_techniczne['podawanie']
        pośrednie += self.cechy_psychiczne['pracowitość']
        pośrednie += self.cechy_fizyczne['wytrzymałość']
        punkty += pośrednie * 2.5  # wagi najwyższe
        pośrednie = 0
        pośrednie += self.cechy_fizyczne['przyśpieszenie']
        pośrednie += self.cechy_psychiczne['przewidywanie']
        pośrednie += self.cechy_psychiczne['stanowczość']
        pośrednie += self.cechy_techniczne['rzuty_wolne']
        pośrednie += self.cechy_techniczne['przeciwdziałanie']
        pośrednie += self.cechy_psychiczne['determinacja']
        pośrednie += self.cechy_psychiczne['odwaga']
        pośrednie += self.cechy_psychiczne['agresja']
        pośrednie += self.cechy_techniczne['technika']
        pośrednie += self.cechy_fizyczne['zwinność']
        pośrednie += self.cechy_psychiczne['kreatywność']
        pośrednie += self.cechy_techniczne['rzuty_rożne']
        pośrednie += self.cechy_techniczne['dryblowanie']
        punkty += pośrednie * 2  # wagi wysokie
        pośrednie = 0
        pośrednie += self.cechy_fizyczne['balans']
        pośrednie += self.cechy_psychiczne['ustawianie']
        pośrednie += self.cechy_fizyczne['szybkość']
        pośrednie += self.cechy_techniczne['główkowanie']
        pośrednie += self.cechy_psychiczne['spryt']
        pośrednie += self.cechy_psychiczne['improwizowanie']
        pośrednie += self.cechy_techniczne['wykańczanie']
        pośrednie += self.cechy_techniczne['rzuty_karne']
        punkty += pośrednie * 1.5  # wagi średnie
        punkty += self.cechy_fizyczne['siła']
        punkty += self.cechy_techniczne['długie_podania']
        punkty += self.cechy_fizyczne['skoczność']
        punkty += self.cechy_psychiczne['autorytet']
        return 0, punkty * 0.25, punkty * 0.5, punkty * 0.25

    def __repr__(self):
        wynik = super().__repr__() + " Pomocnik"
        return wynik


class Napastnik(Piłkarz):
    def __init__(self):
        super().__init__()

    def oblicz_współczynniki(self):
        """ Funkcja do wyliczania poszczególnych współczynników piłkarza
        (by uprościć algorytmy wyznaczające wyniki meczów). """
        # wszystkie cechy przemnożymy przez jakiś współczynnik w zależności od
        # pozycji piłkarza (niektóre cechy bardziej przydatne na danej pozycji)

        punkty = 0
        pośrednie = 0
        pośrednie += self.cechy_fizyczne['kondycja']
        pośrednie += self.cechy_techniczne['przechodzenie']
        pośrednie += self.cechy_fizyczne['wytrzymałość']
        pośrednie += self.cechy_fizyczne['przyśpieszenie']
        pośrednie += self.cechy_psychiczne['odwaga']
        pośrednie += self.cechy_psychiczne['agresja']
        pośrednie += self.cechy_fizyczne['zwinność']
        pośrednie += self.cechy_psychiczne['kreatywność']
        pośrednie += self.cechy_techniczne['dryblowanie']
        pośrednie += self.cechy_fizyczne['szybkość']
        pośrednie += self.cechy_techniczne['główkowanie']
        pośrednie += self.cechy_psychiczne['spryt']
        pośrednie += self.cechy_psychiczne['improwizowanie']
        pośrednie += self.cechy_techniczne['wykańczanie']
        pośrednie += self.cechy_techniczne['rzuty_karne']
        pośrednie += self.cechy_fizyczne['skoczność']
        punkty += pośrednie * 2.5  # wagi najwyższe
        pośrednie = 0
        pośrednie += self.cechy_psychiczne['koncentracja']
        pośrednie += self.cechy_techniczne['przyjęcie']
        pośrednie += self.cechy_psychiczne['pracowitość']
        pośrednie += self.cechy_psychiczne['ustawianie']
        pośrednie += self.cechy_psychiczne['determinacja']
        pośrednie += self.cechy_techniczne['technika']
        pośrednie += self.cechy_psychiczne['stanowczość']
        pośrednie += self.cechy_psychiczne['autorytet']
        punkty += pośrednie * 2  # wagi wysokie
        pośrednie = 0
        pośrednie += self.cechy_psychiczne['opanowanie']
        pośrednie += self.cechy_psychiczne['współdziałanie']
        pośrednie += self.cechy_techniczne['długie_strzały']
        pośrednie += self.cechy_techniczne['podawanie']
        pośrednie += self.cechy_fizyczne['siła']
        punkty += pośrednie * 1.5  # wagi średnie
        punkty += self.cechy_psychiczne['przewidywanie']
        punkty += self.cechy_techniczne['rzuty_wolne']
        punkty += self.cechy_techniczne['przeciwdziałanie']
        punkty += self.cechy_techniczne['rzuty_rożne']
        punkty += self.cechy_fizyczne['balans']
        punkty += self.cechy_techniczne['długie_podania']
        return 0, punkty * 0.13, punkty * 0.37, punkty * 0.5

    def __repr__(self):
        wynik = super().__repr__() + " Napastnik"
        return wynik


class Trener(Osoba):
    def __init__(self):
        super().__init__()
        self.cechy_trenerskie = cechy.trenerskie()

    def __repr__(self):
        return "Osoba " + str([self.imię, self.nazwisko, self.wzrost]) + \
            " Trener " + str(self.cechy_trenerskie)