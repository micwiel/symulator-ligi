#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from random import randint
import osoby


MINIMALNY_ROZMIAR_DRUŻYNY = 18
MAKSYMALNY_ROZMIAR_DRUŻYNY = 30


class Drużyna:
    def __init__(self, nazwa):
        self.nazwa = nazwa
        self.lista_zawodników = self.twórz_zawodników()
        self.trener = osoby.Trener()
        self.bramka, self.obrona, self.pomoc, self.atak = \
            self.oblicz_współczynniki()
        self.rozegrane_mecze = 0
        self.zwycięstwa = 0
        self.remisy = 0
        self.porażki = 0
        self.bramki_zdobyte = 0
        self.bramki_stracone = 0

    def twórz_zawodników(self):
        # losujemy wielkość drużyny
        liczba_zawodników = randint(MINIMALNY_ROZMIAR_DRUŻYNY,
                                    MAKSYMALNY_ROZMIAR_DRUŻYNY)
        # losujemy liczbę bramkarzy (nie mniej niż 2, nie więcej niż 15% całego
        # zespołu)
        liczba_bramkarzy = randint(2, 0.15 * liczba_zawodników // 1)
        liczba_zawodników -= liczba_bramkarzy
        # losujemy liczbę obrońców (nie mniej niż 4, nie więcej niż 60%
        # pozostałego zespołu)
        liczba_obrońców = randint(4, 0.6 * liczba_zawodników // 1)
        liczba_zawodników -= liczba_obrońców
        # losujemy liczbę pomocników (nie mniej niż 4, nie więcej niż 60%
        # pozostałego zespołu)
        liczba_pomocników = randint(4, 0.6 * liczba_zawodników // 1)
        liczba_zawodników -= liczba_pomocników
        # pozostali to napastnicy
        liczba_napastników = liczba_zawodników

        # generujemy piłkarzy
        lista = []
        for i in range(liczba_bramkarzy):
            lista.append(osoby.Bramkarz())
        for i in range(liczba_obrońców):
            lista.append(osoby.Obrońca())
        for i in range(liczba_pomocników):
            lista.append(osoby.Pomocnik())
        for i in range(liczba_napastników):
            lista.append(osoby.Napastnik())
        return lista

    def oblicz_współczynniki(self):
        """ Funkcja obliczająca współczynniki do uproszczenia algorytmów. """
        bramka = 0
        obrona = 0
        pomoc = 0
        atak = 0
        for i in self.lista_zawodników:
            bramka += i.bramka
            obrona += i.obrona
            pomoc += i.pomoc
            atak += i.atak
        # liczymy średnie (po co? bez również dobrze zadziała)
        bramka = bramka // len(self.lista_zawodników)
        obrona = obrona // len(self.lista_zawodników)
        pomoc = pomoc // len(self.lista_zawodników)
        atak = atak // len(self.lista_zawodników)
        return bramka, obrona, pomoc, atak

    def mecz(self, strzelone, stracone):
        self.rozegrane_mecze += 1
        if strzelone > stracone:
            self.zwycięstwa += 1
        elif strzelone < stracone:
            self.porażki += 1
        else:
            self.remisy += 1
        self.bramki_zdobyte += strzelone
        self.bramki_stracone += stracone

    def punkty(self):
        return 3 * self.zwycięstwa + self.remisy

    def __repr__(self):
        lista = []
#        for i in self.lista_zawodników:
#            lista.append(str(i))
#        return "Drużyna: " + str(self.nazwa) + "\n" + "\n".join(lista) + \
#            "\numiejętności\nbramkarza: " + str(self.bramka) + "\nobrona: " + \
#            str(self.obrona) + "\npomoc: " + str(self.pomoc) + "\natak: " + \
#            str(self.atak) + "\n"

        return "Drużyna " + str(self.nazwa) + "\t" + str(self.rozegrane_mecze)\
            + " " + str(self.punkty()) + " " + str(self.zwycięstwa) + " " + \
            str(self.remisy) + " " + str(self.porażki) + " " + \
            str(self.bramki_zdobyte) + "-" + str(self.bramki_stracone)

    def __lt__(self, drugi):
        if self.punkty() == drugi.punkty():
            return self.bramki_zdobyte - self.bramki_stracone < \
                drugi.bramki_zdobyte - drugi.bramki_stracone
        return self.punkty() < drugi.punkty()