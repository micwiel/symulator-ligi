#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from random import randint
import prymitywny_graf as graf
import math
from druzyna import Drużyna


class Mecz:
    """ Tu będziemy rozgrywać mecze. Znajdą się tu najważniejsze algorytmy. """
    def __init__(self, drużyna1, drużyna2):
        self.drużyna1 = drużyna1
        self.drużyna2 = drużyna2

    def rozegraj(self):
        # bazowanie na sumowaniu współczynników
        a = self.drużyna1.bramka + self.drużyna1.obrona + self.drużyna1.pomoc\
            + self.drużyna1.atak
        b = self.drużyna2.bramka + self.drużyna2.obrona + self.drużyna2.pomoc\
            + self.drużyna2.atak

        aszanse = 0
        bszanse = 0
        if a > b:
            aszanse += 4
        elif a < b:
            bszanse += 4
        if self.drużyna1.bramka > self.drużyna2.bramka:
            bszanse -= 2
        elif self.drużyna1.bramka < self.drużyna2.bramka:
            aszanse -= 2
        if self.drużyna1.obrona > self.drużyna2.obrona:
            bszanse -= 2
        elif self.drużyna1.obrona < self.drużyna2.obrona:
            aszanse -= 2
        if self.drużyna1.pomoc > self.drużyna2.pomoc:
            aszanse += 2
        elif self.drużyna1.pomoc < self.drużyna2.pomoc:
            bszanse += 2
        if self.drużyna1.atak > self.drużyna2.atak:
            aszanse += 2
        elif self.drużyna1.atak < self.drużyna2.atak:
            bszanse += 2

        aszanse += randint(-3, 3)
        bszanse += randint(-3, 3)

        a = self.gole(aszanse)
        b = self.gole(bszanse)

        self.drużyna1.mecz(a, b)
        self.drużyna2.mecz(b, a)

    def gole(self, szanse):
        if szanse < 2:
            return randint(0, 1)
        elif szanse < 4:
            return randint(0, 2)
        elif szanse < 6:
            return randint(0, 3)
        elif szanse < 8:
            return randint(0, 4)
        else:
            return randint(0, 5)

    def __repr__(self):
        return str(self.drużyna1.nazwa) + " " + str(self.drużyna2.nazwa)


class Kolejka:
    def __init__(self, graf):
        self.lista_meczy = []
        lista = []  # lista pomocnicza by nie powtórzyć tych samych drużyn w
        # jednej kolejce
        for i in graf.lista_węzłów:
            lista.append(i)

        while len(lista) > 0:
            węzeł1 = lista[0]  # ściągamy dostępną drużynę
            węzeł2 = graf.następny(węzeł1, lista)  # szukamy dla niej
            # przeciwnika
            lista.remove(węzeł1)  # wywalamy
            lista.remove(węzeł2)
            graf.dodaj_krawędź(węzeł1, węzeł2)  # dodajemy do grafu krawędź
            # symbolizującą rozegranie meczu
            drużyna1 = węzeł1.obiekt
            drużyna2 = węzeł2.obiekt
            self.lista_meczy.append(Mecz(drużyna1, drużyna2))  # wrzucamy mecz

    def rozegraj(self):
        for i in self.lista_meczy:
            i.rozegraj()

    def __repr__(self):
        return str(self.lista_meczy)


class Liga:
    def __init__(self, ilość_drużyn):
        self.graf_drużyn = graf.Graf()  # graf do trzymania drużyn
        for i in range(ilość_drużyn):
            self.graf_drużyn.dodaj_węzeł(Drużyna(i + 1))  # drużyny numerujemy
            # od 1
        self.aktualna_kolejka = 0
        self.lista_kolejek = []
        for i in range(int(ilość_drużyn * (ilość_drużyn - 1) / 2)):
            # !!! ten wzór nie działa. ciekawe dlaczego
            try:  # przez to musimy łapać wyjątek, tymczasowe obejście problemu
                self.lista_kolejek.append(Kolejka(self.graf_drużyn))
            except:
                pass

    def rozegraj(self):
        for i in self.lista_kolejek:
            i.rozegraj()

    def __repr__(self):
        lista1 = []
        for i in self.graf_drużyn.lista_węzłów:
            lista1.append(i.obiekt)
        lista1.sort()
        lista2 = []
        lista2.append("TABELA:")
        lista2.append("drużyna,\tmecze, punkty, zwycięstwa, remisy, porażki, "
            + "bramki")
        while len(lista1) > 0:
            lista2.append(str(lista1.pop()))
        return "\n".join(lista2)


# główny program
#print('Podaj ilość drużyn: ', end='')
#ilość_drużyn = int(input().strip())
ilość_drużyn = 16
# wymuszamy liczby będące potęgą 2 ze względu na problemy z mieszaniem meczy
if ilość_drużyn < 2 or \
        math.log(ilość_drużyn, 2) - int(math.log(ilość_drużyn, 2)) != 0:
    print('Podaj poprawną liczbę.')
    exit()
liga = Liga(ilość_drużyn)
liga.rozegraj()
print(liga)