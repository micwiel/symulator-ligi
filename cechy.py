#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from random import randint


def losuj_parametr(pmax=10, pmin=0):
    """ Funkcja do losowania parametrów. """
    return randint(pmin, pmax)


def fizyczne():
    słownik = {}
    słownik['przyśpieszenie'] = losuj_parametr()
    słownik['zwinność'] = losuj_parametr()
    słownik['balans'] = losuj_parametr()
    słownik['skoczność'] = losuj_parametr()
    słownik['kondycja'] = losuj_parametr()
    słownik['szybkość'] = losuj_parametr()
    słownik['wytrzymałość'] = losuj_parametr()
    słownik['siła'] = losuj_parametr()
    return słownik


def psychiczne():
    słownik = {}
    słownik['agresja'] = losuj_parametr()
    słownik['przewidywanie'] = losuj_parametr()
    słownik['odwaga'] = losuj_parametr()
    słownik['opanowanie'] = losuj_parametr()
    słownik['koncentracja'] = losuj_parametr()
    słownik['kreatywność'] = losuj_parametr()
    słownik['stanowczość'] = losuj_parametr()
    słownik['determinacja'] = losuj_parametr()
    słownik['spryt'] = losuj_parametr()
    słownik['autorytet'] = losuj_parametr()
    słownik['improwizowanie'] = losuj_parametr()
    słownik['ustawianie'] = losuj_parametr()
    słownik['współdziałanie'] = losuj_parametr()
    słownik['pracowitość'] = losuj_parametr()
    return słownik


def techniczne():
    słownik = {}
    słownik['rzuty_rożne'] = losuj_parametr()
    słownik['przechodzenie'] = losuj_parametr()
    słownik['dryblowanie'] = losuj_parametr()
    słownik['wykańczanie'] = losuj_parametr()
    słownik['przyjęcie'] = losuj_parametr()
    słownik['rzuty_wolne'] = losuj_parametr()
    słownik['główkowanie'] = losuj_parametr()
    słownik['długie_strzały'] = losuj_parametr()
    słownik['długie_podania'] = losuj_parametr()
    słownik['podawanie'] = losuj_parametr()
    słownik['rzuty_karne'] = losuj_parametr()
    słownik['przeciwdziałanie'] = losuj_parametr()
    słownik['technika'] = losuj_parametr()
    return słownik


def bramkarskie():
    słownik = {}
    słownik['zachowanie_w_powietrzu'] = losuj_parametr()
    słownik['dominacja'] = losuj_parametr()
    słownik['komunikacja'] = losuj_parametr()
    słownik['ekscentryczność'] = losuj_parametr()
    słownik['łapanie'] = losuj_parametr()
    słownik['kopanie'] = losuj_parametr()
    słownik['sam_na_sam'] = losuj_parametr()
    słownik['refleks'] = losuj_parametr()
    słownik['gwałtowność'] = losuj_parametr()
    słownik['skłonność_puszczania_piłki'] = losuj_parametr()
    słownik['wyrzut'] = losuj_parametr()
    return słownik


def trenerskie():
    słownik = {}
    słownik['atak'] = losuj_parametr()
    słownik['bramkarze'] = losuj_parametr()
    słownik['obrona'] = losuj_parametr()
    słownik['kondycja'] = losuj_parametr()
    słownik['zarządzanie'] = losuj_parametr()
    słownik['wpływ'] = losuj_parametr()
    słownik['taktyka'] = losuj_parametr()
    słownik['technika'] = losuj_parametr()
    słownik['młodzież'] = losuj_parametr()
    słownik['przystosowywanie'] = losuj_parametr()
    słownik['determinacja'] = losuj_parametr()
    słownik['osądzanie'] = losuj_parametr()
    słownik['dyscyplina'] = losuj_parametr()
    słownik['motywowanie'] = losuj_parametr()
    słownik['fizjoterapia'] = losuj_parametr()
    return słownik