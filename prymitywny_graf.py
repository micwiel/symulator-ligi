#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class Węzeł:
    def __init__(self, obiekt):
        self.obiekt = obiekt
        self.lista_sąsiedztwa = []

    def dodaj_sąsiada(self, węzeł):
        self.lista_sąsiedztwa.append(węzeł)

    def liczba_sąsiadów(self):
        return len(self.lista_sąsiedztwa)

    def __repr__(self):
        return str(self.obiekt)


class Graf:
    def __init__(self):
        self.lista_węzłów = []

    def dodaj_węzeł(self, węzeł):
        węzeł = Węzeł(węzeł)
        self.lista_węzłów.append(węzeł)

    def ilość_węzłów(self):
        return len(self.lista_węzłów)

    def dodaj_krawędź(self, węzeł1, węzeł2):
        węzeł1.dodaj_sąsiada(węzeł2)
        węzeł2.dodaj_sąsiada(węzeł1)

    def następny(self, węzeł, lista):
        """ Funkcja wyznaczająca następną dostępną drużynę dla zadanej
            drużyny. """
        lista_p = []  # pomocnicza lista
        for i in lista:  # powielamy sobie
            lista_p.append(i)
        lista_p.remove(węzeł)  # usuwamy węzeł brany pod uwagę
        for i in węzeł.lista_sąsiedztwa:
            try:
                lista_p.remove(i)  # wywalamy z listy węzły, które już nie mogą
                # zostać użyte
            except:  # gdy okaże się że w sąsiadach jest więcej węzłów niż w
            # ogóle bierzemy pod uwagę to przeskakujemy
                pass
        return lista_p[0]  # zwracamy co zostało